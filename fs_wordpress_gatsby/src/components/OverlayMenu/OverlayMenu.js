/* eslint-disable max-len */
import React from 'react';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';
import {useMenuQuery} from '../../hooks/useMenuQuery';
import {useState} from 'react';
import {Overlay} from './OverlayMenu.styles';
import PropTypes from 'prop-types';

const OverlayMenu = ({menuOpen}) => {
  const {wpMenu} = useMenuQuery();
  const menu = wpMenu.menuItems.nodes;
  const i = 0;
  return (
    <Overlay menuOpen={menuOpen}>
      <div className="inner">
        <ul className="overlayMenu">
          {menu.map((mainItem) =>
            !mainItem.parentId ? // pass main item to the click handler so each item gets its own click handler
                clickHandler(mainItem, i) :
              null,
          )}
        </ul>
      </div>
    </Overlay>
  );
};

function clickHandler(mainItem, i) {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);

  function keydown(event) {
    if (event.keyCode === 13) {
      setClick(!click);
    }
  }

  return (
    // build menu item and child menu
    <li
      key={mainItem.id}
      onClick={handleClick}
      onKeyDown={keydown}
      className={click ? 'nav nav-active primaryLink' : 'nav primaryLink'}
    >
      {mainItem.label}
      {mainItem.childItems.nodes.length !== 0 && click ? (
        <div>
          <Icon category="Up Arrow" />
        </div>
      ) : (
        <div>
          <Icon category="Down Arrow" />
        </div>
      )}
      {mainItem.childItems.nodes.length !== 0 ? (
        <ul className={click ? 'active' : ''}>
          {mainItem.childItems.nodes.map((childItem) => (
            <li key={childItem.id}>
              <Link
                to={childItem.url}
                activeClassName="nav-active"
                className="secondaryLink"
              >
                {childItem.label}
              </Link>
            </li>
          ))}
        </ul>
      ) : null}
    </li>
  );
}

OverlayMenu.propTypes = {
  menuOpen: PropTypes.bool,
};

export default OverlayMenu;
