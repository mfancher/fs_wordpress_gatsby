import styled from 'styled-components';

export const Overlay = styled.div`
  width: 100%;
  height: 100%;
  opacity: ${(props) => (props.menuOpen ? '1' : '0')};
  transform: ${(props) =>
    props.menuOpen ? 'translateX(0%)' : 'translateX(-100%)'};
  z-index: 100000;
  top: 75px;
  border: 1px solid var(--lightGrey);
  left: 0px;
  transition: all 0.3s ease;
  display: none;

  .inner {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: var(--darkGrey);
    width: 100%;
    height: 100%;
    color: var(--illiniDarkBlue);

    .invertedLogo {
      max-width: 200px;
      margin: 0 0 60px 0;
    }

    .nav {
      width: 100%;
      padding: 0.5rem;
      border-top: 1px solid grey;
      display: flex;
    }

    .nav-active {
      background: #fff;
      color: var(--illiniOrange);
    }
    .nav div {
      margin-left: 1rem;
    }
    .active {
      display: block !important;
      width: 100%;
      background: #fff;
    }

    .active li {
      background: #fff;
      list-style: none;
    }

    .nav ul {
      display: none;
    }

    .overlayMenu {
      text-align: left;
      list-style-type: none;
      margin: 0;
      width: 100%;
    }
  }

  .closeButton {
    position: absolute;
    top: 50px;
    right: 50px;
    color: #fff;
    width: 30px;
    height: 30px;
    cursor: pointer;
    transition: all 1s ease;
    outline: none;

    :hover {
      transform: rotate(180deg);
    }
  }
`;
