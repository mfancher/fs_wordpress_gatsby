import styled from 'styled-components';
import {GatsbyImage} from 'gatsby-plugin-image';

export const Container = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  margin: 0 auto;
  margin-top: 3rem;
  margin-bottom: 3rem;

  .left {
    display: flex;
    flex-wrap: wrap;
  }

  .icon {
    height: 2rem;
    width: 2rem;
    padding: 0.5rem;
  }

  .headline img {
    margin-left: 0.25rem;
  }

  .btn {
    margin: 0 auto;
    margin-top: 4rem;
  }
  p {
    margin-bottom: 0px;
  }

  @media (max-width: 991px) {
  }

  @media (max-width: 480px) {
  }
`;

export const StyledImg = styled(GatsbyImage)`
  max-width: 50%;
  overflow: hidden;
  z-index: 0;
`;

export const Title = styled.p`
  text-align: center;
  padding-top: 3rem;
  padding-bottom: 3rem;
`;
