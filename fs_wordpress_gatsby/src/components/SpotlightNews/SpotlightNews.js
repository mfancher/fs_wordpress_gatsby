import React from 'react';
import {Card} from '../Card/Card';
import {Container, Title, StyledImg} from './SpotlightNews.styles';
import {getImage} from 'gatsby-plugin-image';
import {useSpotlightNewsQuery} from '../../hooks/useSpotlightNewsQuery';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';

const SpotlightNews = () => {
  const {allWpNews} = useSpotlightNewsQuery();
  const data = allWpNews.nodes;

  return (
    <>
      <Title className="secondaryTitle">Featured News</Title>
      <Container>
        {data.map((news) =>
          news.acf_news.spotlightNews ? (
            <Card
              key={news.slug}
              width="700px"
              justify="space-between"
              content={
                <>
                  <div>
                    <div className="left">
                      <Icon
                        className="icon"
                        category={news.acf_news.category}
                      ></Icon>
                      <p className="secondaryLink">{news.acf_news.category}</p>
                    </div>

                    <Link to={`/News/${news.slug}`}>
                      <p className="headline">
                        {news.acf_news.title}
                        <Icon alt="" category="Chevron Right"></Icon>
                      </p>
                    </Link>
                    <br />
                    <br />
                    <p className="detail">{news.date}</p>
                  </div>
                  <StyledImg
                    alt={news.acf_news.featuredImage.altText}
                    image={getImage(news.acf_news.featuredImage.localFile)}
                  ></StyledImg>
                </>
              }
            ></Card>
          ) : null,
        )}
      </Container>
    </>
  );
};

export default SpotlightNews;
