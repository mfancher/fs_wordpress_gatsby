import React, {useState} from 'react';
import {GlobalStyles, Primary, Wrapper} from './Layout.styles';
import Header from '../Header/Header';
import Hamburger from '../Hamburger/Hamburger';
import OverlayMenu from '../OverlayMenu/OverlayMenu';
import Footer from '../Footer/Footer';
import PropTypes from 'prop-types';

const Layout = ({children}) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const handleOverlayMenu = () => setMenuOpen((prev) => !prev);
  return (
    <>
      <GlobalStyles />
      <Hamburger handleOverlayMenu={handleOverlayMenu} menuOpen={menuOpen} />
      <Wrapper>
        <Header />
        <OverlayMenu
          menuOpen={menuOpen}
          callback={handleOverlayMenu}
        ></OverlayMenu>
        <Primary>{children}</Primary>
        <Footer />
      </Wrapper>
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.any.isRequired,
};

export default Layout;
