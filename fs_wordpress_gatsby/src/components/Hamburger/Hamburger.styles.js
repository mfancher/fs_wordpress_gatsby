import styled from 'styled-components';

export const HamburgerButton = styled.div`
  position: absolute;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  z-index: 100000;
  right: 0.5rem;
  top: 1.25rem;
  width: 60px;
  -webkit-align-items: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: space-around;
  -webkit-justify-content: space-around;
  -ms-flex-pack: space-around;
  justify-content: space-around;
  height: 20px;
  cursor: pointer;
  -webkit-transition: all 0.2s ease;
  transition: all 0.2s ease;
  margin-right: 10px;

  img {
    display: block;
    height: 12px;
    width: auto;
    cursor: pointer;
    transition: all 0.2s ease;
    margin-bottom: 0 !important;
    margin-left: 10px;

    :hover {
      transform: scale(1.2);
    }
  }

  @media (min-width: 992px) {
    display: none;
  }
`;
