import * as React from 'react';
import {HamburgerButton} from './Hamburger.styles';
import HamburgerIcon from '../../images/Hamburger_icon.png';
import CloseIcon from '../../images/icon-x.svg';
import PropTypes from 'prop-types';


const Hamburger = ({handleOverlayMenu, menuOpen}) => (
  <HamburgerButton onClick={handleOverlayMenu}>
    <div className="primaryLink">Menu</div>{' '}
    {menuOpen ? (
      <img src={CloseIcon} alt="menu-hamburger" />
    ) : (
      <img src={HamburgerIcon} alt="menu-hamburger" />
    )}
  </HamburgerButton>
);

Hamburger.propTypes = {
  handleOverlayMenu: PropTypes.func,
  menuOpen: PropTypes.bool,
};

export default Hamburger;
