import React from 'react';
import {Container} from './ProviderList.styles';
import {Button} from '../Button/Button';
import chevronRight from '../../images/icon-chevron-right.png';

const ProviderList = (providers) => {
  const data = providers.providers;
  const providerButtons = data.map((provider) => (
    <Button
      key={provider.slug}
      link={`/Providers/${provider.slug}`}
      label={provider.acf_providers.providerName}
      buttonStyle="btn--grey--bg"
      buttonSize="btn--large"
      font="primaryLink"
      iconRight={chevronRight}
    ></Button>
  ));

  return (
    <>
      <span className="primaryLabel">Providers</span>
      <br />
      <Container>{providerButtons}</Container>
    </>
  );
};

export default ProviderList;
