import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  text-align: center;
  margin-top: 1rem;

  .btn {
    margin: 0px;
    margin-right: 0.5rem;
  }
`;
