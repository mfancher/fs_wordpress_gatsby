/* eslint-disable max-len */
import React from 'react';
import {Card} from '../Card/Card';
import {Container, Title, StyledImg} from './SpotlightService.styles';
import {getImage} from 'gatsby-plugin-image';
import {useSpotlightServiceQuery} from '../../hooks/useSpotlightServiceQuery';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';

const SpotlightService = () => {
  const {allWpService} = useSpotlightServiceQuery();
  const data = allWpService.nodes;

  return (
    <>
      <Title className="secondaryTitle">Spotlight Service</Title>
      <Container>
        {data.map((service) =>
          service.acf_services.spotlight ? (
            <Card
              key={service.slug}
              width="400px"
              content={
                <>
                  <div className="iconContainer">
                    <Icon
                      className="icon"
                      category={service.acf_services.category}
                    ></Icon>
                  </div>
                  <div className="titleContainer">
                    <p className="secondaryLink">
                      {service.acf_services.category}
                    </p>
                    <Link to={`/Services/${service.slug}`}>
                      <p className="headline">
                        {service.acf_services.serviceTitle}{' '}
                        <Icon category="Chevron Right"></Icon>
                      </p>
                    </Link>
                  </div>
                  <br />
                  {service.acf_services.spotlightImage && (<><StyledImg
                    alt={service.acf_services.spotlightImage.altText}
                    image={getImage(
                        service.acf_services.spotlightImage.localFile,
                    )}
                  ></StyledImg></>) }
                </>
              }
            ></Card>
          ) : null,
        )}
      </Container>
    </>
  );
};

export default SpotlightService;
