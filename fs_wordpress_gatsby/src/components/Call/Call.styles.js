import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  color: var(--white);
  height: fit-content;
  padding: 20px;
  background: var(--illiniDarkBlue);

  .div1 {
    width: 80%;
    margin: 0 auto;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
  }
  .div1 p {
    margin-bottom: 0;
    margin-right: 20px;
  }
`;
