import React from 'react';
import {Button} from '../Button/Button';
import {Wrapper} from './Call.styles';
import phone from '../../images/icon-phone.png';

const Call = () => {
  return (
    <Wrapper>
      <div className="div1">
        <p className="primaryLink">
          Call the service office for any urgent or after hours requests
        </p>
        <Button
          label="Call 217-333-0340"
          buttonStyle="btn--blue--bg--orange--border"
          buttonSize="btn--large"
          font="primaryLink"
          iconRight={phone}
          link="tel:2173330340"
          externalLink={true}
        ></Button>
      </div>
    </Wrapper>
  );
};

export default Call;
