/* eslint-disable new-cap */
import React from 'react';
import Card from '../Card/Card';
import {Container} from './ServiceSteps.styles';
import Parser from 'html-react-parser';

const ServiceSteps = (service) => {
  console.log(service);
  const informationStep = service.Service.informationStep;
  const responsibilityStep = service.Service.responsibilityStep;
  const workStep = service.Service.workStep;
  const moreInfo = service.Service.moreInfo;

  return (
    <Container>
      <Card
        content={
          <>
            <div className="number">
              <span className="headline">1</span>
            </div>
            <h2 className="tertiaryTitle">
              Who is responsible for this service?
            </h2>
            <br />
            <div className="stepContent paragraph">
              {Parser(responsibilityStep)}
            </div>
          </>
        }
        width="100%"
      ></Card>
      <Card
        content={
          <>
            <div className="number">
              <span className="headline">2</span>
            </div>
            <h2 className="tertiaryTitle">Determine who to use.</h2>
            <br />
            <div className="stepContent paragraph">{Parser(workStep)}</div>
          </>
        }
        width="100%"
      ></Card>
      {informationStep ? (
        <>
          {' '}
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">3</span>
                </div>
                <h2 className="tertiaryTitle">
                  Gather required documentation to submit a request.
                </h2>
                <br />
                <div className="stepContent paragraph">
                  {Parser(informationStep)}
                </div>
              </>
            }
            width="100%"
          ></Card>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">4</span>
                </div>
                <h2 className="tertiaryTitle">Get a login to portal.</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    If you havent already gotten portal credentials, do that
                    now.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">5</span>
                </div>
                <h2 className="tertiaryTitle">Make request in portal.</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    If you have login credentials and all of the necessary
                    information and budget, submit your request now.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">6</span>
                </div>
                <h2 className="tertiaryTitle">What to expect next</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    All service requests will receive and initial communication
                    within 48 hours, which will contain an approximate date of
                    service. If you need further explanation, please use the
                    contact card at the top of this page.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <br />
        </>
      ) : (
        <>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">3</span>
                </div>
                <h2 className="tertiaryTitle">Get a login to portal.</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    If you havent already gotten portal credentials, do that
                    now.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">4</span>
                </div>
                <h2 className="tertiaryTitle">Make request in portal.</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    If you have login credentials and all of the necessary
                    information and budget, submit your request now.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <Card
            content={
              <>
                <div className="number">
                  <span className="headline">5</span>
                </div>
                <h2 className="tertiaryTitle">What to expect next</h2>
                <br />
                <div className="stepContent paragraph">
                  <p>
                    All service requests will receive and initial communication
                    within 48 hours, which will contain an approximate date of
                    service. If you need further explanation, please use the
                    contact card at the top of this page.
                  </p>
                </div>
              </>
            }
            width="100%"
          ></Card>
          <br />
        </>
      )}

      {moreInfo ? (
        <div className="moreInfo">
          <h2 className="tertiaryTitle moreInfoTitle">More Information</h2>
          <br />
          {Parser(moreInfo)}
          <br />
        </div>
      ) : null}
    </Container>
  );
};

export default ServiceSteps;
