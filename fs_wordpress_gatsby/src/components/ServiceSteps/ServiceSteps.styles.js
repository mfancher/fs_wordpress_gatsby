import styled from 'styled-components';


export const Container = styled.div`
  display:flex;
  flex-direction:row;
  justify-content:flex-start;
  align-items:center;
  flex-wrap: wrap;
  text-align:center;
  margin-top:1rem;
  margin-bottom:2rem;
  
  .btn{
    margin:0px;
    margin-right:.5rem;
    
  }
  .number{
  width: 3rem;
  border: 2px solid var(--illiniOrange);
  border-radius: 40px;
  height: 3rem;
  justify-content: center;
  align-content: baseline;
  display: flex;
}

  .number span{
    margin-top:10px;
  }

  h2{
    padding-left: 1rem;
    padding-top: 0.25rem;
  }

  .stepContent{
    width:100%;
    text-align: left;
    margin-left: 4rem;
  }

  .moreInfo{
    text-align:left;
    width:100%;
    margin-top:2rem;
  }

  .moreInfoTitle{
    padding-left:0rem;
  }

  > div{
    margin-top:2rem;
  }
  @media only screen and (max-width:992px){
    
      padding:3rem;
  }

  @media only screen and (max-width:480px){
    
    padding:1.5rem;
}
  

`;
