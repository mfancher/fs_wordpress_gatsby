import React from 'react';
import './Button.css';
import {Link} from 'gatsby';
import PropTypes from 'prop-types';

const STYLES = [
  'btn--blue',
  'btn--orange',
  'btn--grey',
  'btn--blue--bg',
  'btn--blue--bg--orange--border',
  'btn--grey--bg',
  'btn--transparent',
  'btn--white',
];

const SIZES = ['btn--large', 'btn--small'];

export const Button = ({
  label,
  type,
  handleClick,
  buttonStyle,
  buttonSize,
  font,
  iconLeft,
  iconRight,
  link,
  externalLink,
}) => {
  const checkButtonStyle = STYLES.includes(buttonStyle) ?
    buttonStyle :
    STYLES[0];
  const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

  return externalLink ? (
    <>
      <a
        href={link}
        className={
          'btn ' + checkButtonStyle + ' ' + checkButtonSize + ' ' + font
        }
        onClick={handleClick}
        type={type}
      >
        <div>
          {iconLeft ? <img alt="" src={iconLeft} /> : ''}
          {label}
        </div>
        {iconRight ? <img alt="" src={iconRight} /> : ''}
      </a>
    </>
  ) : (
    <Link
      to={link}
      className={'btn ' + checkButtonStyle + ' ' + checkButtonSize + ' ' + font}
      onClick={handleClick}
      type={type}
    >
      <div>
        {iconLeft ? <img alt="" src={iconLeft} /> : ''}
        {label}
      </div>
      {iconRight ? <img alt="" src={iconRight} /> : ''}
    </Link>
  );
};


Button.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  handleClick: PropTypes.string,
  buttonStyle: PropTypes.string,
  buttonSize: PropTypes.string,
  font: PropTypes.string,
  iconLeft: PropTypes.string,
  iconRight: PropTypes.string,
  link: PropTypes.string,
  externalLink: PropTypes.string,

};

export default Button;
