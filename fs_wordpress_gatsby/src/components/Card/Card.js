import React from 'react';
import {CardContainer} from './Card.styles';
import PropTypes from 'prop-types';


export const Card = ({width, content, justify}) => {
  return (
    <>
      <CardContainer
        style={{width: `${width}`, justifyContent: `${justify}`}}
      >
        {content}
      </CardContainer>
    </>
  );
};

Card.propTypes = {

  width: PropTypes.string,
  content: PropTypes.node.isRequired,
  justify: PropTypes.string,
};

export default Card;
