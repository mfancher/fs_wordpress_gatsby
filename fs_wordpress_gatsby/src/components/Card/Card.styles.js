import styled from 'styled-components';

export const CardContainer = styled.div`
  border-radius: 8px;
  padding: 1.5rem;
  background: var(--white);
  display: flex;
  flex-wrap: wrap;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
  margin-right: 1rem;

  .relatedService {
    display: flex;
    width: 100%;
    height: 50px;
  }

  .relatedService img {
    height: 2rem;
    width: auto;
    padding: 0.5rem;
  }

  .relatedService a {
    padding-left: 0.5rem;
  }
`;
