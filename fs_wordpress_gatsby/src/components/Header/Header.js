import React from 'react';
import {Link} from 'gatsby';
import Logo from '../../images/block-i-white-background.png';
import {useMenuQuery} from '../../hooks/useMenuQuery';
import {
  Wrapper,
  Content,
  MainContent,
  NavContent,
  UIUCContent,
  LogoImage,
} from './Header.styles';
import Navigation from '../Navigation/Navigation';
import {Button} from '../Button/Button';
import search from '../../images/icon-search.png';

const Header = () => {
  const {site, wpMenu} = useMenuQuery();

  return (
    <Wrapper>
      <UIUCContent>
        <a href="https://illinois.edu/">
          <p>University of Illinois Urbana-Champaign</p>
        </a>
      </UIUCContent>
      <MainContent>
        <Content>
          <div>
            <a href="https://illinois.edu/">
              <LogoImage src={Logo} alt={site.siteMetadata.title} />
            </a>
            <Link to="/">
              <p className="unitTitleLarge">Facilities & Services</p>
            </Link>
          </div>
          <div className="center">
            <Button
              label="Request a Service"
              buttonStyle="btn--blue"
              buttonSize="btn--large"
              font="primaryLink"
              link="/"
            ></Button>
            <Button
              label="Search"
              buttonStyle="btn--grey"
              buttonSize="btn--large"
              font="secondaryLink"
              link="/"
              icon={search}
            ></Button>
          </div>
        </Content>
      </MainContent>
      <NavContent>
        <Navigation menu={wpMenu.menuItems.nodes} />
      </NavContent>
    </Wrapper>
  );
};

export default Header;
