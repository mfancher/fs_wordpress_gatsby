import React from 'react';
import maintenance from '../../images/icon-maintenance.png';
import facility from '../../images/icon-facilityResources.png';
import rental from '../../images/icon-rentals.png';
import IT from '../../images/icon-itSupport.png';
import supplies from '../../images/icon-supplies.png';
import training from '../../images/icon-training.png';
import collaboration from '../../images/icon-collaboration.png';
import transportation from '../../images/icon-transportation.png';
import newBuild from '../../images/icon-projects-newBuild.png';
import closures from '../../images/icon-roadClosures.png';
import projectUpdates from '../../images/icon-projectUpdates.png';
import organization from '../../images/icon-organization.png';
import strategicInitiative from '../../images/icon-tips.png';
import shopSpotlight from '../../images/icon-shop.png';
import eventsAndTraining from '../../images/icon-events.png';
import providers from '../../images/icon-providers.png';
import DownArrow from '../../images/icon-arrow-down.png';
import UpArrow from '../../images/icon-arrow-up.png';
import chevronRight from '../../images/icon-chevron-right.png';
import phone from '../../images/icon-phone.png';
import email from '../../images/icon-email.png';
import PropTypes from 'prop-types';

export const Icon = ({category, className, width}) => {
  let img = null;

  switch (category) {
    case 'Maintenance + Construction':
      img = maintenance;
      break;
    case 'Facility Resources':
      img = facility;
      break;
    case 'Rentals':
      img = rental;
      break;
    case 'Supplies':
      img = supplies;
      break;
    case 'Training':
      img = training;
      break;
    case 'Transportation':
      img = transportation;
      break;
    case 'Collaboration':
      img = collaboration;
      break;
    case 'Information Technology':
      img = IT;
      break;
    case 'New Build':
      img = newBuild;
      break;
    case 'Closures':
      img = closures;
      break;
    case 'Strategic Initiatives':
      img = strategicInitiative;
      break;
    case 'Events and Training':
      img = eventsAndTraining;
      break;
    case 'Shop Spotlight':
      img = shopSpotlight;
      break;
    case 'Project Updates':
      img = projectUpdates;
      break;
    case 'F&S Organization':
      img = organization;
      break;
    case 'Provider':
    case 'Providers':
      img = providers;
      break;
    case 'Up Arrow':
      img = UpArrow;
      break;
    case 'Down Arrow':
      img = DownArrow;
      break;
    case 'Chevron Right':
      img = chevronRight;
      break;
    case 'Phone':
      img = phone;
      break;
    case 'Email':
      img = email;
      break;
    default:
      img = null;
  }

  return (
    <img
      alt=""
      style={width && {width: `${width}`}}
      className={className && className}
      src={img}
    />
  );
};

Icon.propTypes = {
  category: PropTypes.string.isRequired,
  className: PropTypes.string,
  width: PropTypes.string,
};

export default Icon;
