import React from 'react';
import {Container, StyledImg} from './RelatedNews.styles';
import {getImage} from 'gatsby-plugin-image';
import {Card} from '../Card/Card';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';

const RelatedNews = (news) => {
  const data = news.News;
  const newsButtons = data.map((news) => (
    <Card
      width="550px"
      justify="space-between"
      key={news.slug}
      content={
        <>
          <div className="left">
            <div className="leftInner">
              <Icon className="icon" category={news.acf_news.category}></Icon>
              <p className="secondaryLink">{news.acf_news.category}</p>
            </div>

            <Link to={`/News/${news.slug}`}>
              <p className="headline">
                {news.acf_news.title} <Icon category="Chevron Right"></Icon>
              </p>
            </Link>
            <br />
            <br />
            <p className="detail">{news.date}</p>
          </div>
          <StyledImg
            image={getImage(news.acf_news.featuredImage.localFile)}
          ></StyledImg>
        </>
      }
    ></Card>
  ));

  return (
    <>
      <Container>{newsButtons}</Container>
    </>
  );
};

export default RelatedNews;
