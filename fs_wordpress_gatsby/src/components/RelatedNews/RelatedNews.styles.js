import styled from 'styled-components';
import {GatsbyImage} from 'gatsby-plugin-image';

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  margin: 0 auto;
  margin-top: 3rem;
  margin-bottom: 3rem;

  > div {
    margin-bottom: 2rem;
  }

  .left {
    display: flex;
    flex-wrap: wrap;
    width: 60%;
  }

  .leftInner {
    display: flex;
    flex-wrap: wrap;
  }

  .icon {
    height: 2rem;
    width: 2rem;
    padding: 0.5rem;
  }

  .btn {
    margin: 0 auto;
    margin-top: 4rem;
  }
  p {
    margin-bottom: 0px;
  }

  @media (max-width: 991px) {
  }

  @media (max-width: 480px) {
  }
`;

export const StyledImg = styled(GatsbyImage)`
  max-width: 40%;
  overflow: hidden;
  z-index: 0;
`;

export const Title = styled.p`
  text-align: center;
  padding-top: 3rem;
  padding-bottom: 3rem;
`;
