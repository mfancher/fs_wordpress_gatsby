import styled from 'styled-components';
import {GatsbyImage} from 'gatsby-plugin-image';

export const StyledImg = styled(GatsbyImage)`
  max-height: 1648px;
  width: 100%;
  z-index: 0;
`;

export const Wrapper = styled.div`
  position: relative;
  height: fit-content;
  width: 100%;
  isolation: isolate;
  display: flex;
`;

export const HeaderWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
  height: 100%;
  width: 100%;

  h1 {
    color: var(--white);
    text-align: center;
    line-height: 3rem;
  }
  h2 {
    color: var(--white);
    text-align: center;
  }
  div {
    width: 80%;
    margin: 0 auto;
    display: block;
  }
  div > div {
    width: 50%;
    background: var(--illiniDarkBlue);
    color: var(--white);
    text-align: center;
    border-radius: 16px;
    padding: 15px;
  }
  div > div > div {
    width: 100%;
    border-radius: 8px;
    margin-top: 10px;
    background: var(--white);
    color: var(--darkestGrey);
    text-align: left;
    border-radius: 1rem;
    padding: 0.5rem;
    display: flex;
    flex-flow: row;
    justify-content: flex-end;
  }

  button > div {
    width: 100%;
  }

  .primaryLabel > div > a > div {
    width: 100%;
  }
`;

export const BlendWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;

  .blendOne {
    height: 100%;
    width: 100%;
    background: var(--illiniDarkBlue);
    mix-blend-mode: lighten;
    z-index: 1;
  }

  .blendTwo {
    height: 100%;
    width: 100%;
    background: rgba(19, 41, 75, 0.4);
    background-blend-mode: color;
    mix-blend-mode: normal;
    z-index: 2;
  }
`;
