import React from 'react';
import {getImage} from 'gatsby-plugin-image';
import {useHomepageHeroQuery} from '../../hooks/useHomepageHeroQuery';
import {
  Wrapper,
  HeaderWrapper,
  StyledImg,
  BlendWrapper,
} from './HomepageHero.styles';
import {Button} from '../Button/Button';
import search from '../../images/icon-search.png';

const HomepageHero = () => {
  const {
    wpPage: {acf_homepage: data},
  } = useHomepageHeroQuery();
  const imageData = getImage(data.hero.localFile);
  return (
    <Wrapper>
      <StyledImg
        image={imageData}
        alt="Hero image showing F&S Worker using tool on wall"
      />
      <BlendWrapper>
        <div className="blendOne"></div>
      </BlendWrapper>
      <BlendWrapper>
        <div className="blendTwo"></div>
      </BlendWrapper>
      <HeaderWrapper>
        <div>
          <h1 className="primaryTitle">{data.heroText}</h1>
          <h2 className="paragraphLarge">{data.heroTextSmall}</h2>
          <div className="primaryLabel">
            <span>Search for a service, document, project, and more</span>
            <div>
              <Button
                label="Search"
                buttonStyle="btn--blue--bg"
                buttonSize="btn--large"
                font="primaryLink"
                link="/"
                icon={search}
              ></Button>
            </div>
          </div>
        </div>
      </HeaderWrapper>
    </Wrapper>
  );
};

export default HomepageHero;
