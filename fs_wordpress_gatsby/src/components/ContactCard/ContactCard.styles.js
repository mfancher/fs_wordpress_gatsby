import styled from 'styled-components';

export const Container = styled.div`
  background: var(--illiniDarkBlue);
  border: 2px solid var(--illiniDarkBlue);
  border-radius: 8px 8px 0px 0px;
  padding-left: 1.5rem;
  padding-right: 1.5rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;
export const LowerContainer = styled.div`
  background: var(--white);
  border-radius: 0px 0px 8px 8px;
  padding: 1.5rem;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
`;
