import React from 'react';
import {Container, LowerContainer} from './ContactCard.styles';
import {Icon} from '../Icon/Icon';
import PropTypes from 'prop-types';

export const ContactCard = ({
  width,
  name,
  provider,
  email,
  phone,
  moreInfo,
}) => {
  return (
    <>
      <Container style={{width: `${width}`}}>
        <span className="primaryLabel whiteTxt">Contact Information</span>
      </Container>
      <LowerContainer style={{width: `${width}`}}>
        <span className="headline">{name}</span>
        <br />
        <span className="paragraph darkBlueTxt">{provider}</span>
        <br />
        <br />
        <Icon width="20px" className="imgMargin" category="Phone"></Icon>
        <span className="primaryLink darkBlueTxt">{phone}</span>
        <br />
        <Icon alt="" width="24px" className="imgMargin" category="Email"></Icon>
        <span className="primaryLink darkBlueTxt">{email}</span>
        <br />
        <p className="paragraphSmall">{moreInfo}</p>
      </LowerContainer>
    </>
  );
};

ContactCard.propTypes = {
  width: PropTypes.string,
  name: PropTypes.string.isRequired,
  provider: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  moreInfo: PropTypes.string,

};

export default ContactCard;
