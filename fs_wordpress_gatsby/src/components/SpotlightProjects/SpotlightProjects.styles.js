import styled from 'styled-components';
import {GatsbyImage} from 'gatsby-plugin-image';

export const Container = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  margin: 0 auto;
  margin-top: 3rem;
  margin-bottom: 3rem;

  .icon {
    width: 100%;
    height: auto;
  }

  .iconContainer {
    width: 56px;
    height: 56px;
    padding: 0.75rem;
    background: var(--darkGrey);
    margin-right: 1rem;
  }

  .titleContainer {
    max-width: 250px;
  }

  .titleContainer img {
    padding-left: 0.25rem;
  }

  p {
    margin-bottom: 0px;
  }

  @media (max-width: 991px) {
  }

  @media (max-width: 480px) {
  }
`;

export const StyledImg = styled(GatsbyImage)`
  height: 220px;
  width: auto;
  overflow: hidden;
  z-index: 0;
  margin-top: 1.5rem;
`;

export const Title = styled.p`
  text-align: center;
  padding-top: 3rem;
  padding-bottom: 3rem;
`;
