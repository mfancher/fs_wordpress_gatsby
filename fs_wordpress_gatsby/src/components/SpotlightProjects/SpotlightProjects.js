import React from 'react';
import {Card} from '../Card/Card';
import {Container, Title, StyledImg} from './SpotlightProjects.styles';
import {getImage} from 'gatsby-plugin-image';
import {useSpotlightProjectQuery} from '../../hooks/useSpotlightProject';
import {Icon} from '../Icon/Icon';
import {Link} from 'gatsby';

const SpotlightProjects = () => {
  const {allWpProject} = useSpotlightProjectQuery();
  const data = allWpProject.nodes;

  return (
    <>
      <Title className="secondaryTitle">Spotlight Projects</Title>
      <Container>
        {data.map((project) =>
          project.acf_project.spotlight ? (
            <Card
              key={project.slug}
              width="400px"
              content={
                <>
                  <div className="iconContainer">
                    <Icon
                      className="icon"
                      category={project.acf_project.category}
                    ></Icon>
                  </div>
                  <div className="titleContainer">
                    <p className="secondaryLink">
                      {project.acf_project.category}
                    </p>
                    <Link to={`/Projects/${project.slug}`}>
                      <p className="headline">
                        {project.acf_project.title}
                        <Icon category="Chevron Right"></Icon>
                      </p>
                    </Link>
                  </div>
                  <br />
                  <StyledImg
                    alt={project.acf_project.featuredImage.altText}
                    image={getImage(
                        project.acf_project.featuredImage.localFile,
                    )}
                  ></StyledImg>
                </>
              }
            ></Card>
          ) : null,
        )}
      </Container>
    </>
  );
};

export default SpotlightProjects;
