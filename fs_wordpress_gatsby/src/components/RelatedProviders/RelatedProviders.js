import React from 'react';
import {Container, StyledImg} from './RelatedProviders.styles';
import {Card} from '../Card/Card';
import {getImage} from 'gatsby-plugin-image';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';

const RelatedProviders = (providers) => {
  const data = providers.Providers;
  const providerButtons = data.map((provider) => (
    <Card
      width="100%"
      justify="space-between"
      key={provider.slug}
      content={
        <>
          <div className="left">
            <div className="leftInner">
              <Icon className="icon" category="Providers" />
              <p className="secondaryLink">Providers</p>
            </div>

            <Link to={`/Providers/${provider.slug}`}>
              <p className="headline">
                {provider.acf_providers.providerName}{' '}
                <Icon category="Chevron Right" />
              </p>
            </Link>

            <br />
            <p className="paragraph">{provider.acf_providers.description}</p>
          </div>
          <StyledImg
            image={getImage(provider.acf_providers.image.localFile)}
          ></StyledImg>
        </>
      }
    ></Card>
  ));

  return (
    <>
      <Container>{providerButtons}</Container>
    </>
  );
};

export default RelatedProviders;
