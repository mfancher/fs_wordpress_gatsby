import React from 'react';
import {Container} from './RelatedServices.styles';
import {Card} from '../Card/Card';
import {Link} from 'gatsby';
import {Icon} from '../Icon/Icon';

const RelatedServices = (services) => {
  const data = services.Service;
  const data2 = services.Services;

  const serviceButtons = data ?
    data.map((service) => (
      <Card
        key={service.slug}
        width="400px"
        content={
          <>
            <div className="relatedService">
              <Icon category={service.acf_services.category} />
              <p className="secondaryLink">{service.acf_services.category}</p>
            </div>
            <br />
            <br />{' '}
            <div className="relatedService">
              <Link to={`/Services/${service.slug}`}>
                <p className="headline">
                  {service.acf_services.serviceTitle}
                </p>
              </Link>
              <Icon category="Chevron Right" />
            </div>
          </>
        }
      ></Card>
    )) :
    data2.map((service) => (
      <Card
        key={service.slug}
        width="400px"
        content={
          <>
            <div className="relatedService">
              <Icon category={service.acf_services.category} />
              <p className="secondaryLink">{service.acf_services.category}</p>
            </div>
            <br />
            <br />{' '}
            <div className="relatedService">
              <Link to={`/Services/${service.slug}`}>
                <p className="headline">
                  {service.acf_services.serviceTitle}
                </p>
              </Link>
              <Icon category="Chevron Right" />
            </div>
          </>
        }
      ></Card>
    ));

  return (
    <>
      <Container>{serviceButtons}</Container>
    </>
  );
};

export default RelatedServices;
