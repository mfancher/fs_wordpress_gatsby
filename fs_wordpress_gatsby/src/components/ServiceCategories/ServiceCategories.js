import React from 'react';
import {Card} from '../Card/Card';
import {Container} from './ServiceCategories.styles';
import {useServiceCategoriesQuery} from '../../hooks/useServiceCategoriesQuery';
import {Button} from '../Button/Button';
import chevronRight from '../../images/icon-chevron-right.png';
import maintenance from '../../images/icon-maintenance.png';
import facility from '../../images/icon-facilityResources.png';
import rental from '../../images/icon-rentals.png';
import IT from '../../images/icon-itSupport.png';
import supplies from '../../images/icon-supplies.png';
import training from '../../images/icon-training.png';
import collaboration from '../../images/icon-collaboration.png';
import transportation from '../../images/icon-transportation.png';

const ServiceCategories = () => {
  const {allWpService} = useServiceCategoriesQuery();
  const services = allWpService.distinct;

  const serviceButtons = services.map((service) => (
    <Button
      key={service}
      label={service}
      link="/"
      buttonStyle="btn--grey--bg"
      buttonSize="btn--large"
      font="primaryLink"
      iconRight={chevronRight}
      iconLeft={iconHandler(service)}
    ></Button>
  ));

  return (
    <>
      <Container>
        <Card
          width="100%"
          content={
            <>
              <p className="tertiaryTitle">What do you need help with?</p>
              {serviceButtons}
            </>
          }
        ></Card>
        <Button
          label="View all Services"
          iconRight={chevronRight}
          buttonSize="btn--large"
          buttonStyle="btn--transparent"
          font="primaryLink"
          link="/"
        ></Button>
      </Container>
    </>
  );
};

function iconHandler(category) {
  let img = null;

  switch (category) {
    case 'Maintenance + Construction':
      img = maintenance;
      break;
    case 'Facility Resources':
      img = facility;
      break;
    case 'Rentals':
      img = rental;
      break;
    case 'Supplies':
      img = supplies;
      break;
    case 'Training':
      img = training;
      break;
    case 'Transportation':
      img = transportation;
      break;
    case 'Collaboration':
      img = collaboration;
      break;
    case 'Information Technology':
      img = IT;
      break;
    default:
      img = null;
  }

  return img;
}

export default ServiceCategories;
