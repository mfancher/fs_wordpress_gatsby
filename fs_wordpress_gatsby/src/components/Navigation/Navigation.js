/* eslint-disable max-len */
import React from 'react';
import {Link} from 'gatsby';
import {Wrapper} from './Navigation.styles';
import Icon from '../Icon/Icon';
import {useState} from 'react';
import PropTypes from 'prop-types';

function NavBar({menu}) {
  const i = 0;
  return (
    <Wrapper>
      <ul>
        {menu.map((mainItem) =>
          !mainItem.parentId ? // necessary for each component to get it's own clickhandler
              clickHandler(mainItem, i) :
            null,
        )}
      </ul>
    </Wrapper>
  );
}

function clickHandler(mainItem, i) {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);

  function keydown(event) {
    if (event.keyCode === 13) {
      setClick(!click);
    }
  }

  return (
    // build menu item and child menu
    <li
      key={mainItem.id}
      onClick={handleClick}
      onKeyDown={keydown}
      tabIndex={i}
      className={click ? 'nav nav-active' : 'nav'}
    >
      {mainItem.label}
      {mainItem.childItems.nodes.length !== 0 && click ? (
        <div>
          <Icon className="icon" category="Up Arrow" />
        </div>
      ) : (
        <div>
          <Icon className="icon" category="Down Arrow" />
        </div>
      )}
      {mainItem.childItems.nodes.length !== 0 ? (
        <ul className={click ? 'active' : ''}>
          {mainItem.childItems.nodes.map((childItem) => (
            <li key={childItem.id}>
              <Link
                to={childItem.url}
                activeClassName="nav-active"
                className="secondaryLink"
              >
                {childItem.label}
              </Link>
            </li>
          ))}
        </ul>
      ) : null}
    </li>
  );
}

NavBar.propTypes = {
  menu: PropTypes.array,
};

export default NavBar;
