import styled from 'styled-components';

export const Wrapper = styled.nav`
  font-family: 'Source Sans Pro';
  font-size: 1rem;
  font-weight: 700;
  width: 100%;
  max-width: 1180px;
  margin: 0 auto;
  height: 100%;
  display: flex;
  justify-content: flex-start;

  .img{
    width:12px;
  }
  .nav-active {
    text-decoration: underline;
    color:var(--illiniOrange);
  }
  ul{
    margin-left:0px;

  }
  @media (min-width: 992px) {
    font-size: 0.85rem;
    display: flex;
  }
  
  @media(max-width:991px){
    display:none;
  }

  @media (min-width: 1200px) {
    font-size: 1rem;
  }

  ul li {
    display: flex;
    justify-content:space-between;
    margin: 0 10px 0 0;
    float: left;
    height: 50px;
    position: relative;
    padding-inline: 10px;
    padding-top: 10px;
    
}

    a {
      display: flex;
      padding: 0 0 0 10px;
      transition: all 0.2s ease;

      div {
        margin: -4px 0 0 5px;
      }
    }
  }

  ul li:last-child {
    margin: 0;
  }

  ul li > ul {
    display: none;
  }

  .active{
    display:block;
  }

  .nav-active{
    color: var(--illiniOrange)!important;
    background-color:#fff;
    text-decoraction:underline;

  }

  ul ul li a:hover{
    color: var(--illiniOrange)!important;
    text-decoraction:underline!important;
  }

  .nav{
    cursor:pointer;
  }

  ul li div img{
   margin-left:8px;
   margin-bottom: 2px;
 }


  ul ul {
    animation: fadeInMenu 0.3s both ease-in;
    display: none;
    position: absolute;
    left: 0;
    margin: 0;
    top: 50px;
    text-transform: none;
    background: #fff;
    padding: 15px 10px 10px 10px;
    box-shadow: 0px 2px 16px rgba(0, 0, 0, 0.15);
    border-radius: 8px;
    min-width:200px;
    max-width:250px;
  }

  ul ul li {
    width: auto;
    min-width: 170px;
  }

  ul ul li a {
    padding: 5px 10px;
    white-space: nowrap;
  }
  

  @keyframes fadeInMenu {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
