/* eslint-disable max-len */
import * as React from 'react';
import Layout from '../components/Layout/Layout';
import HomepageHero from '../components/HomepageHero/HomepageHero';
import Call from '../components/Call/Call';
import SpotlightService from '../components/SpotlightService/SpotlightService';
import ServiceCategories from '../components/ServiceCategories/ServiceCategories';
import SpotlightNews from '../components/SpotlightNews/SpotlightNews';
import SpotlightProjects from '../components/SpotlightProjects/SpotlightProjects';

// import Seo from "../components/seo/seo" <Seo title="Home" />

const IndexPage = () => (
  <Layout>
    <HomepageHero />
    <Call />
    <SpotlightService />
    <ServiceCategories />
    <SpotlightNews />
    <SpotlightProjects />
  </Layout>
);

export default IndexPage;
