/* eslint-disable new-cap */
/* eslint-disable max-len */
import * as React from 'react';
import Layout from '../../components/Layout/Layout.js';
import {graphql} from 'gatsby';
import Parser from 'html-react-parser';
import {GatsbyImage, getImage} from 'gatsby-plugin-image';
import RelatedNews from '../../components/RelatedNews/RelatedNews';
import RelatedProviders from '../../components/RelatedProviders/RelatedProviders';
import {Icon} from '../../components/Icon/Icon';

const ProjectPage = (data) => {
  const root = data.data.wpProject.acf_project;
  const title = root.title;
  const summary = root.description;
  const completedDate = root.completedDate;
  const startDate = root.startDate;
  const category = root.category;
  const image = root.featuredImage.localFile;
  const imageAlt = root.featuredImage.altText;
  const imageData = getImage(image);
  const longDescription = root.story;
  const relatedNews = root.otherNews;
  const relatedProviders = root.providers;

  return (
    <Layout>
      <div className="whitebg">
        <div className="container">
          <div className="newsTop">
            <Icon className="imgMargin" category={category}></Icon>
            <span className="primaryLinkLarge">{category}</span>
            <br />
            <h1 className="primaryTitle">{title}</h1>
            <p className="paragraph greyDarkestTxt">
              Started: {startDate}
              <br />
              Completed: {completedDate}
            </p>
            <p className="paragraph greyDarkestTxt">{summary}</p>
            <GatsbyImage
              alt={imageAlt}
              className="newsImage"
              image={imageData}
            ></GatsbyImage>
          </div>
        </div>
      </div>
      <div className="container newsBottom">
        <div style={{width: '100%'}}>{Parser(longDescription)}</div>
      </div>
      {relatedProviders ? (
        <div className="container">
          <p className="secondaryTitle">
            Learn more about the providers on this project
          </p>
          <RelatedProviders providers={relatedProviders} />
        </div>
      ) : null}
      {relatedNews ? (
        <div className="container">
          <p className="secondaryTitle">
            Learn more about the providers on this project
          </p>
          <RelatedNews news={relatedNews} />
        </div>
      ) : null}
    </Layout>
  );
};

export default ProjectPage;

export const query = graphql`
  query ProjectQuery($id: String) {
    wpProject(id: { eq: $id }) {
      acf_project {
        completedDate
        startDate
        description
        story
        title
        featuredImage {
          localFile {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
          }
          altText
        }
        relatedProviders {
          ... on WpProvider {
            id
            acf_providers {
              providerName
              description
            }
            slug
          }
        }
        category
      }
      slug
    }
  }
`;
