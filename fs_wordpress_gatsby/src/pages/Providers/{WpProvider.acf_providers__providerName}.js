/* eslint-disable new-cap */
import * as React from 'react';
import Layout from '../../components/Layout/Layout.js';
import {graphql} from 'gatsby';
import {ContactCard} from '../../components/ContactCard/ContactCard';
import Parser from 'html-react-parser';
import {GatsbyImage, getImage} from 'gatsby-plugin-image';
import RelatedServices from '../../components/RelatedServices/RelatedServices';
import {Icon} from '../../components/Icon/Icon';

const ProviderPage = (data) => {
  const root = data.data.wpProvider.acf_providers;
  const title = root.providerName;
  const longDescription = root.whatWeDo;
  const description = root.description;
  const contact = root.contact.acf_contacts;
  const relatedServices = root.relatedServices;
  const image = root.image.localFile;
  const imageAlt = root.image.altText;
  const imageData = getImage(image);

  return (
    <Layout>
      <div className="whitebg">
        <div className="container">
          <div className="serviceTop">
            <Icon className="imgMargin" alt="" category="Providers"></Icon>
            <span className="primaryLinkLarge">Providers</span>
            <br />
            <h1 className="primaryTitle">{title}</h1>
            <p className="paragraph greyDarkestTxt">{description}</p>
          </div>
          <div>
            <ContactCard
              width="300px"
              name={contact.name}
              phone={contact.phoneNumber}
              email={contact.email}
              provider={title}
            ></ContactCard>
          </div>
          <div style={{width: '100%'}}></div>
          <GatsbyImage
            className="providerImage"
            image={imageData}
            alt={imageAlt}
          ></GatsbyImage>
          <div style={{width: '100%'}}>{Parser(longDescription)}</div>
        </div>
      </div>
      {relatedServices ? (
        <div className="container">
          <h2 className="tertiaryTitle">Featured Services</h2>
          <RelatedServices services={relatedServices}></RelatedServices>
        </div>
      ) : null}
    </Layout>
  );
};

export default ProviderPage;

export const query = graphql`
  query ProviderQuery($id: String) {
    wpProvider(id: { eq: $id }) {
      acf_providers {
        providerName
        relatedServices {
          ... on WpService {
            id
            acf_services {
              serviceTitle
              category
            }
            slug
          }
        }
        whatWeDo
        description
        image {
          localFile {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
          }
          altText
        }
        contact {
          ... on WpContact {
            id
            acf_contacts {
              email
              name
              phoneNumber
            }
          }
        }
      }
      slug
    }
  }
`;
