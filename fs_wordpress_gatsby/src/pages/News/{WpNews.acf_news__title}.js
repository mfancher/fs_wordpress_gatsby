/* eslint-disable new-cap */
/* eslint-disable max-len */
import * as React from 'react';
import Layout from '../../components/Layout/Layout.js';
import {graphql} from 'gatsby';
import Parser from 'html-react-parser';
import {GatsbyImage, getImage} from 'gatsby-plugin-image';
import RelatedServices from '../../components/RelatedServices/RelatedServices';
import RelatedNews from '../../components/RelatedNews/RelatedNews';
import RelatedProviders from '../../components/RelatedProviders/RelatedProviders';
import {Icon} from '../../components/Icon/Icon';

const NewsPage = (data) => {
  const root = data.data.wpNews.acf_news;
  const title = root.title;
  const summary = root.summary;
  const date = data.data.wpNews.date;
  const longDescription = root.articleText;
  const category = root.category;
  const image = root.featuredImage.localFile;
  const imageAlt = root.featuredImage.altText;
  const imageData = getImage(image);
  const relatedServices = root.services;
  const relatedNews = root.otherNews;
  const relatedProviders = root.providers;

  return (
    <Layout>
      <div className="whitebg">
        <div className="container">
          <div className="newsTop">
            <Icon className="imgMargin" category={category}></Icon>
            <span className="primaryLinkLarge">{category}</span>
            <br />
            <h1 className="primaryTitle">{title}</h1>
            <p className="paragraph greyDarkestTxt">{date}</p>
            <p className="paragraph greyDarkestTxt">{summary}</p>
            <GatsbyImage
              alt={imageAlt}
              className="newsImage"
              image={imageData}
            ></GatsbyImage>
          </div>
        </div>
      </div>
      <div className="container newsBottom">
        <div style={{width: '100%'}}>{Parser(longDescription)}</div>
      </div>
      {relatedProviders ? (
        <div className="container">
          <p className="secondaryTitle">Learn more about these providers</p>
          <RelatedProviders providers={relatedProviders} />
        </div>
      ) : null}
      {relatedServices ? (
        <div className="container">
          <p className="secondaryTitle">Related Services</p>
          <RelatedServices services={relatedServices} />
        </div>
      ) : null}
      {relatedNews ? (
        <div className="container">
          <p className="secondaryTitle">Related News</p>
          <RelatedNews news={relatedNews} />
        </div>
      ) : null}
    </Layout>
  );
};

export default NewsPage;

export const query = graphql`
  query NewsQuery($id: String) {
    wpNews(id: { eq: $id }) {
      acf_news {
        articleText
        otherNews {
          ... on WpNews {
            id
            date(formatString: "MMM DD, YYYY | h:mm a")
            acf_news {
              title
              category
              sunsetDate
              articleText
              featuredImage {
                localFile {
                  childImageSharp {
                    gatsbyImageData(placeholder: TRACED_SVG)
                  }
                }
              }
            }
          }
        }
        project {
          ... on WpProject {
            id
          }
        }
        providers {
          ... on WpProvider {
            id
            acf_providers {
              providerName
              image {
                localFile {
                  childImageSharp {
                    gatsbyImageData(placeholder: TRACED_SVG)
                  }
                }
              }
              description
            }
            slug
          }
        }
        services {
          ... on WpService {
            id
            acf_services {
              serviceTitle
              category
            }
            slug
          }
        }
        title
        sunsetDate
        featuredImage {
          localFile {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
          }
          altText
        }
        category
        summary
      }
      slug
      date(formatString: "MMM DD, YYYY | h:mm a")
    }
  }
`;
