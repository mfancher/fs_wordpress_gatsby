import * as React from 'react';
import Layout from '../../components/Layout/Layout.js';
import {graphql} from 'gatsby';
import {ContactCard} from '../../components/ContactCard/ContactCard';
import {Icon} from '../../components/Icon/Icon';
import ProviderList from '../../components/ProviderList/ProviderList';
import ServiceSteps from '../../components/ServiceSteps/ServiceSteps';
import RelatedServices from '../../components/RelatedServices/RelatedServices';

const ServicePage = (data) => {
  const root = data.data.wpService.acf_services;
  const title = root.serviceTitle;
  const category = root.category;
  const description = root.description;
  const providers = root.providers;
  const contact = providers[0].acf_providers.contact.acf_contacts;
  const relatedServices = root.relatedServices;
  console.log({root});
  return (
    <Layout>
      <div className="whitebg">
        <div className="container">
          <div className="serviceTop">
            <Icon className="imgMargin" category={category}></Icon>
            <span className="primaryLinkLarge">{category}</span>
            <br />
            <h1 className="primaryTitle">{title}</h1>
            <p className="paragraph greyDarkestTxt">{description}</p>
          </div>
          <div>
            <ContactCard
              width="300px"
              name={contact.name}
              phone={contact.phoneNumber}
              email={contact.email}
              provider={providers[0].acf_providers.providerName}
            ></ContactCard>
          </div>
          <div style={{width: '100%'}}>
            <ProviderList providers={providers}></ProviderList>
          </div>
        </div>
      </div>
      <div className="container">
        <ServiceSteps service={root}></ServiceSteps>
      </div>
      {relatedServices ? (
        <div className="whitebg">
          <div className="container">
            <p className="tertiaryTitle">Explore Related Services</p>
            <RelatedServices service={relatedServices}></RelatedServices>
          </div>
        </div>
      ) : null}
    </Layout>
  );
};

export default ServicePage;

export const query = graphql`
  query ServiceQuery($id: String) {
    wpService(id: { eq: $id }) {
      acf_services {
        category
        description
        serviceTitle
        spotlight
        moreInfo
        subCategory
        responsibilityStep
        workStep
        informationStep
        providers {
          ... on WpProvider {
            id
            slug
            acf_providers {
              providerName
              contact {
                ... on WpContact {
                  id
                  acf_contacts {
                    name
                    phoneNumber
                    email
                  }
                }
              }
            }
          }
        }
        relatedServices {
          ... on WpService {
            id
            acf_services {
              serviceTitle
              category
            }
            slug
          }
        }
      }
    }
  }
`;
