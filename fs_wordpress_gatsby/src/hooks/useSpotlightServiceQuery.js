import {useStaticQuery, graphql} from 'gatsby';

export const useSpotlightServiceQuery = () => {
  const data = useStaticQuery(graphql`
    query SpotlightService {
      allWpService {
        nodes {
          acf_services {
            serviceTitle
            spotlight
            spotlightImage {
              localFile {
                childImageSharp {
                  gatsbyImageData(placeholder: TRACED_SVG)
                }
              }
              altText
            }
            category
          }
          slug
        }
      }
    }
  `);
  return data;
};
