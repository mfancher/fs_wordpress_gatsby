import {useStaticQuery, graphql} from 'gatsby';

export const useSpotlightNewsQuery = () => {
  const data = useStaticQuery(graphql`
    query SpotlightNews {
      allWpNews {
        nodes {
          acf_news {
            title
            featuredImage {
              localFile {
                childImageSharp {
                  gatsbyImageData(placeholder: TRACED_SVG)
                }
              }
              altText
            }
            category
            spotlightNews
          }
          slug
          date(formatString: "MMM DD, YYYY | h:mm a")
        }
      }
    }
  `);
  return data;
};
