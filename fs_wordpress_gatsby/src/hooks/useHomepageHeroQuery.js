import {useStaticQuery, graphql} from 'gatsby';

export const useHomepageHeroQuery = () => {
  const data = useStaticQuery(graphql`
    query HomepageHero {
      wpPage(databaseId: { eq: 115 }) {
        databaseId
        title
        id
        acf_homepage {
          heroText
          heroTextSmall
          hero {
            localFile {
              childImageSharp {
                gatsbyImageData(placeholder: TRACED_SVG)
              }
            }
          }
        }
      }
    }
  `);
  return data;
};
