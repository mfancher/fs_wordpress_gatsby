import {useStaticQuery, graphql} from 'gatsby';

export const useMenuQuery = () => {
  const data = useStaticQuery(graphql`
    query HeaderQuery {
      wpMenu(name: { eq: "mainMenu" }) {
        name
        menuItems {
          nodes {
            url
            label
            parentId
            id
            childItems {
              nodes {
                id
                label
                url
              }
            }
          }
        }
      }
      site {
        siteMetadata {
          author
          title
        }
      }
    }
  `);

  return data;
};
