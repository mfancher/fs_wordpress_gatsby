import {useStaticQuery, graphql} from 'gatsby';

export const useServiceCategoriesQuery = () => {
  const data = useStaticQuery(graphql`
    query ServiceCategories {
      allWpService {
        distinct(field: acf_services___category)
      }
    }
  `);
  return data;
};
