import {useStaticQuery, graphql} from 'gatsby';

export const useSpotlightProjectQuery = () => {
  const data = useStaticQuery(graphql`
    query SpotlightProjectQuery {
      allWpProject {
        nodes {
          acf_project {
            category
            title
            spotlight
            featuredImage {
              altText
              localFile {
                childImageSharp {
                  gatsbyImageData(placeholder: TRACED_SVG)
                }
              }
            }
          }
          slug
        }
      }
    }
  `);
  return data;
};
